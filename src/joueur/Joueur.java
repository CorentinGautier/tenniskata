package src.joueur;

public class Joueur {

    private String name;

    public Joueur(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "name='" + name + '\'' +
                '}';
    }
}
