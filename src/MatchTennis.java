package src;

import src.joueur.Joueur;

import java.util.Scanner;
import java.util.*;

public class MatchTennis {
    Scanner sc = new Scanner(System.in);


    public void initJoueurs(){
        System.out.println("Quelle est votre nom ?");
        String strNom = sc.nextLine();
        Joueur joueur1 = new Joueur(strNom);
        System.out.println("Bonjour "+ joueur1.getName());
    }

    public static void main(String[] args) {
        System.out.println("Bienvenue à vous !");
    }
}
